<?php include_once ROOT . '/templates/header.tpl.php'; ?>

<div class="container">

    <? if (!$empty): ?>

        <p>Сортировать по &nbsp;|&nbsp;
            <? foreach ($sort as $name => $link): ?>
                <a href="<?= $link ?>"><?= $name ?></a>&nbsp;|&nbsp;
            <? endforeach; ?>
        </p>

        <? foreach ($tasks as $task): ?>
        <div class="tasks list-group">
            <div class="row list-group-item">
                <div class="col-md-1 col-lg-1">
                    <?= $task['id']; ?>
                </div>
                <div class="col-md-7 col-lg-7 align-middle" >
                    <p>Имя пользователя: <?= $task['name'] ?></p>
                    <p>Email: <?= $task['email'] ?></p>
                    <div id="response"></div>
                    <div class="clear"></div>
                    <p>Текст задачи: <?= $task['text'] ?></p>
                    <p>Статус задачи: <?= $task['status'] == 1 ?
                            '<span style="color: green;font-weight: bold">Выполнено</span>' : '<span style="color:#d72424;font-weight: bold">Не выполнено</span>' ?>
                    </p>
                    <? if ($task['admin_edit']): ?>
                        <p style="font-style: italic">отредактировано администратором</p>
                    <? endif; ?>
                    <? if ($isAdmin): ?>
                        <a class="btn btn-primary" href="/index.php?view=task&action=edit&id=<?= $task['id'] ?>">Редактировать</a>
                        <a class="btn btn-primary" href="/index.php?view=task&action=delete&id=<?= $task['id'] ?>">Удалить</a>
                    <? endif; ?>
                </div>
            </div>
        </div>
        <? endforeach; ?>
    <? else: ?>
    <p>Задачи еще созданы. Добавить <a href="index.php?view=task&action=add">задачу</a>.</p>
    <? endif; ?>

    <? if ($pagerShow): ?>
    <ul class="pagination">
        <? foreach ($queries as $index => $query): ?>
            <? if ($active == $index): ?>
                    <li class="active"><span class="inline-block"><?= $index; ?></span></li>
            <? else: ?>
                    <li><a href="<?= $query ?>" class="inline-block"><?= $index; ?></a></li>
            <? endif; ?>
        <? endforeach; ?>
    </ul>
    <? endif; ?>
</div>

</body>

