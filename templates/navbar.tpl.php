<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <p class="navbar-brand">Задачник</p>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <? if ($isAdmin): ?>
            <ul class="nav navbar-nav">
                <li><a href="/">Главная</a></li>
                <li><a href="/index.php?view=task&action=add">Добавить задачу</a></li>
                <li><a href="/index.php?action=logout">Выход</a></li>
                <li><span style="position: relative;top: 15px;">Привет, <?= $authName; ?></span></li>
                <li><a href="/index.php?action=clear">Очистить задачи и юзеров</a></li>
            </ul>

            <? else: ?>
                <ul class="nav navbar-nav">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/index.php?view=task&action=add">Добавить задачу</a></li>
                    <li><a href="/index.php?view=login">Вход</a></li>
                    <li><a href="/index.php?action=clear">Очистить задачи и юзеров</a></li>
                </ul>
            <? endif; ?>
        </div>
    </div>
</nav>

<? if ($message): ?>
    <div class="container">
        <div class="alert alert-<?= $message['type'] ?>" role="alert">
            <?= $message['message']; ?>
        </div>
    </div>
<? endif; ?>