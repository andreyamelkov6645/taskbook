<?php include_once ROOT . '/templates/header.tpl.php'; ?>


<div class="container">
    <h1><?= $title; ?></h1>
    <form action="<?= $action; ?>" method="post">
        <div class="form-group">
            <label for="name" title="Четыре буквы, цифры или _">Имя пользователя:</label>
            <input type="text" class="form-control" name="name" id="name"
                   title="Четыре буквы, цифры или _"
                   value=""
            >
        </div>
        <? if ($action == '/index.php?action=registration'): ?>
        <div class="form-group">
            <label for="name" title="Четыре буквы, цифры или _">Email:</label>
            <input type="text" class="form-control" name="email" id="name"
                   title="Четыре буквы, цифры или _"
                   value=""
            >
        </div>
        <? endif; ?>

        <div class="form-group">
            <label for="pwd" title="Минимум шесть знаков">Пароль:</label>
            <input type="password" class="form-control" name="pwd" id="pwd" title="Минимум шесть знаков">
        </div>
        <input type="hidden" name="reg_form_sent" value="1">
        <button type="submit" class="btn btn-default"><?= $button; ?></button>
    </form>
</div>

</body>

