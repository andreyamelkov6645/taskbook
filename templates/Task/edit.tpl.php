<?php include_once ROOT . '/templates/header.tpl.php'; ?>


<div class="container">
    <h1>Редактирование задачи номер <?= $task['id']; ?></h1>
    <form action="/index.php?action=update" method="post" id="upload_form" enctype="multipart/form-data" >

        <div class="form-group col-lg-7 col-md-7 ">
            <label for="name" title="">Имя:</label>
            <input type="text" class="form-control" name="name" value="<?= $name ?>"
                   title="">
        </div>
        <div class="form-group col-lg-7 col-md-7 ">
            <label for="email" title="Валидный адрес почты">Электронная почта:</label>
            <input type="text" class="form-control" name="email" id="email"  value="<?= $task['email'] ?>"
                   title="Валидный адрес почты">
        </div>
        <div class="form-group col-lg-7 col-md-7 ">
            <label for="task_text" title="Минимум один, максимум 2000 знаков">Текст:</label>
            <textarea cols="80" rows="10" class="form-control" name="task_text" id="task_text"
                      title="Минимум один, максимум 2000 знаков"><?= $task['text'] ?></textarea>
        </div>
        <div class="form-group col-lg-7 col-md-7 ">
            <label for="status" title="Статус задачи">Выполнено:</label>
            <input style="position: relative;top: 3px;left: 5px;" type="checkbox" <?

                $status = '';
                if ($task['status']) {
                    $status = $task['status'] ? " checked " : "";
                }

                if (
                    isset($_SESSION['task']['id']) &&
                    $_SESSION['task']['id'] == $task['id']
                ) {
                    $status = $_SESSION['task']['status'] ? " checked " : "";
                }

                print $status;

                ?> class="" name="task_status" id="status"
                   title="Статус задачи">
        </div>
        <div class="form-group col-lg-7 col-md-7">

            <div class="form-group">
                <input type="hidden" id="task_id" name="task_id" value="<?= $task['id'] ?>">
                <input type="hidden" name="edit_form_sent" value="1">

            </div>

            <button type="submit" class="btn btn-primary">Сохранить</button>
            <a href="/" class="btn btn-primary">Вернуться</a>
        </div>
    </form>

    <script src="/jquery.cookie.js"></script>
    <script type="application/javascript">
        $('#status').change(function () {
            $.post("index.php?action=save-edit", {
                status: Number($('#status').prop('checked')),
                id: $('#task_id').val(),
            });
        });
    </script>

</div>
</body>