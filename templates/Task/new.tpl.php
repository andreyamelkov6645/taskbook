<?php include_once ROOT . '/templates/header.tpl.php'; ?>


<div class="container">
    <h1>Добавить задачу</h1>
    <form action="<?= $action ?>" method="post" id="upload_form" enctype="multipart/form-data" >

        <div class="form-group col-lg-7 col-md-7 ">
            <label for="name" title="">Имя:</label>
            <input type="text" class="form-control" name="name" value="<?= $name; ?>"
                   title="">
        </div>
        <div class="form-group col-lg-7 col-md-7 ">
            <label for="email" title="Валидный адрес почты">Электронная почта:</label>
            <input type="text" class="form-control" name="email" id="email"  value="<?= $email; ?>"
                   title="Валидный адрес почты">
        </div>
        <div class="form-group col-lg-7 col-md-7 ">
            <label for="task_text" title="Минимум один, максимум 2000 знаков">Текст:</label>
            <textarea cols="80" rows="10" class="form-control" name="task_text" id="task_text"
                      title="Минимум один, максимум 2000 знаков"></textarea>
        </div>
        <div class="form-group col-lg-7 col-md-7 ">
            <label for="status" title="Статус задачи">Выполнено:</label>
            <input style="position: relative;top: 3px;left: 5px;" type="checkbox" class="" name="task_status" id="status"
                   title="Статус задачи">
        </div>
        <div class="form-group col-lg-7 col-md-7">
            <input type="hidden" name="new_task_sent" value="1">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>

    </form>

</div>
</body>