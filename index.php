<?php

define('ROOT', dirname(__FILE__));

include_once (ROOT . '/src/TaskBook.php');

$TaskBook = new TaskBookController();
$TaskBook->run();