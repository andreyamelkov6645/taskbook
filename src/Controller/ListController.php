<?php

/**
 * Главная страница, список задач.
 *
 * Class ListController
 */
class ListController extends PageController {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Главная страница.
     */
    function start() {

        $view = new ListView();
        $taskModel = new Task();
        $pager = new Pager();

        $offset = 0;

        $limit = Pager::ITEMS_IN_PAGE;
        $start = $offset * $limit;

        $page = isset($_GET['page']) && intval($_GET['page']) > 1 ? $_GET['page'] : 1;
        $params = [
            'start' => $start,
            'limit' => $limit,
            'sort' => isset($_GET['sort']) ? $_GET['sort'] : 'id',
            'sortBy' => isset($_GET['sortby']) ? $_GET['sortby'] : 'DESC',
            'page' => $page,
        ];

        $entriesCount = $taskModel->getCount();

        // Рендер списка задач.
        $view->render('List/list', [
            'tasks' => $taskModel->getTasks($params),
            'queries' => $pager::getQueries($_GET, $entriesCount),
            'active' => $page,
            'pagerShow' => $entriesCount > $limit,
            'sort' => $view->getSortList(),
            'empty' => $entriesCount == 0,
        ]);
    }
}