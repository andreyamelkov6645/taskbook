<?php

/**
 * Авторизация.
 *
 * Class RegController
 */
class RegController extends PageController
{

    /**
     *  Ник администратора.
     *
     * @var string
     */
    private $admin;

    public function __construct()
    {
        parent::__construct();
        $this->admin = 'admin';
    }

    /**
     * Страница авторизации.
     */
    public function loginPage()
    {
        $view = new PageView();
        $view->render(
            'Reg/auth-form',
            [
                'title' => 'Авторизация',
                'button' => 'Вход',
                'action' => '/index.php?action=auth',
            ]
        );
    }

    /**
     * Выход.
     */
    public function logoutAction()
    {
        $_SESSION = [];
        session_destroy();
        $this->redirect('/');
    }

    /**
     * Авторизация.
     */
    function authAction()
    {

        if (!Page::validateValues($_POST, 'login')) {
            $this->redirect('view=login');
        } else {

            $name = $_POST['name'];
            $pwd = $_POST['pwd'];

            $user = new User();
            $name = $user->checkLogin($name, $pwd);

            if ($name) {
                $_SESSION["is_auth"] = TRUE;
                $_SESSION["name"] = $name;
                Page::addMessage("Вы вошли как " . $name);
                $this->redirect('/');
            } else {
                Page::addMessage("Неверные логин либо пароль.", 'danger');
                $this->redirect('view=login');
            }
        }
    }

}