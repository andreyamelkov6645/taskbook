<?php

/**
 * Задачи.
 *
 * Class TaskController
 */
class TaskController extends PageController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Удалить задачу.
     *
     * @param $taskID - ID задачи.
     * @throws Exception
     */
    public function deleteAction($taskID)
    {
        $user = new User();
        if (!$user->isAdmin()) {
            Page::addMessage('Для удаления авторизуйтесь как администратор.');
            $this->redirect(http_build_query([
                'view' => 'login',
            ]));
        }

        $task = new Task();
        $task->deleteTask($taskID);
        Page::addMessage("Задача " . $taskID . " удалена");
        $this->redirect('/');
    }

    /**
     * Страница редактирование задачи.
     *
     * @param $taskID - ID задачи.
     * @throws Exception
     */
    public function editPage($taskID)
    {
        $view = new PageView();
        $task = new Task();
        $user = new User();

        if (!$user->isAdmin()) {
            Page::addMessage('Для редактирования авторизуйтесь как администратор.');
            $this->redirect(http_build_query([
                'view' => 'login',
            ]));
        }

        $taskObj = $task->getTask($taskID);
        if ($taskID) {
            $view->render('Task/edit', [
                'task' => $taskObj,
                'name' => $user->getUserNameByTaskID($taskID),
            ]);
        } else {
            throw new \Exception("Broken task ID.");
        }
    }

    /**
     * Обновление задачи.
     *
     * @param $values - POST данные.
     * @throws Exception
     */
    public function updateAction($values)
    {
        $user = new User();
        if (!$user->isAdmin()) {
            Page::addMessage('Для редактирования авторизуйтесь как администратор.');
            $this->redirect(http_build_query([
                'view' => 'login',
            ]));
        }

        if (Page::validateEmail($values['email'])) {
            if (
                isset($values['task_text']) &&
                isset($values['task_id']) &&
                isset($values['edit_form_sent'])
            ) {
                $task = new Task();
                $task->updateTask($values);
                Page::addMessage("Задача " . $values['task_id'] . " обновлена");
                $this->redirect('/');
            } else {
                throw new \Exception("Broken POST values.");
            }
        } else {
            $this->redirect(http_build_query([
                'view' => 'task',
                'action' => 'edit',
                'id' => $values['task_id'],
            ]));
        }
    }

    /**
     * Страница добавления задачи.
     */
    public function addPage()
    {
        $view = new PageView();
        $user = new User();
        $view->render('Task/new', [
            'email' => $user->getEmail(),
            'name' => $user->getName(),
            'action' => 'index.php?action=addtask',
        ]);
    }

    /**
     * Очистить задачи и юеров.
     */
    function clearAction()
    {
        $task = new Task();
        $user = new User();

        $task->deleteTasks();
        $user->deleteUsers();

        Page::addMessage('Задачи и юзеры удалены.');
        $this->redirect('/');
    }

    /**
     * Сохрание статуса задачи после перезагрузки страницы.
     *
     * @param $values - POST данные.
     */
    function saveEditAction($values)
    {
        $task = new Task();
        $task->saveData($values);
    }

    /**
     * Добавление задачи.
     *
     * @param $values - POST данные.
     */
    function newTaskAction($values)
    {
        $task = new Task();

        if (!Page::validateValues($values, 'task')) {
            $this->redirect('view=task&action=add');
        }
        if (Page::validateEmail($values['email'])) {
            $task->addTask($values);
            $this->redirect('/');
        } else {
            $this->redirect('view=task&action=add');
        }
    }

}