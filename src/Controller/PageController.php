<?php


class PageController
{

    public $front;

    protected function __construct()
    {
        $this->front = 'http://' . $_SERVER["HTTP_HOST"] . '/index.php';
    }

    /**
     * Редирект.
     *
     * @param $address
     */
    function redirect($address)
    {
        if ($address == '/') {
            header('Location: ' . '/', true, 303);
        }
        else {
            header('Location: ' . $this->front . '?' . $address, true, 303);
        }
        exit();
    }

}