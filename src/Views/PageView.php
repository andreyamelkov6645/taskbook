<?php

/**
 * Вьюх рендеринга страниц.
 *
 * Class PageView
 */
class PageView {

    protected $params;

    public function __construct()
    {
        $userModel = new User();
        $params = [
            'isAdmin' => $userModel->isAdmin(),
            'isLogged' => $userModel->isLogged(),
            'authName' => $userModel->getName(),
        ];
        $this->params = $params;
    }

    /**
     * Рендер шаблона с входнными переменными.
     *
     * @param $template - шаблон.
     * @param array $params - переменные в шаблноне.
     */
    function render($template, $params = [])
    {
        header('Content-type: text/html; charset=utf-8');

        $params = array_merge($this->params, $params);
        if (!empty($params) && is_array($params)) {
            foreach ($params as $key => $param) {
                $$key = $param;
            }
        }

        $message = isset($_SESSION['message']) ? $_SESSION['message'] : FALSE;
        unset($_SESSION['message']);

        include_once ROOT. '/templates/'. $template . '.tpl.php';

    }
}