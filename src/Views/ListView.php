<?php

/**
 * Вьюз.
 *
 * Class ListView
 */
class ListView extends PageView {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Рендерит ссылки для сортировки.
     */
    function getSortList() {
        $sort = [];
        foreach ([
            'name' => 'Имя',
            'email' => 'Email',
            'status' => 'Статус',
        ] as $sortBy => $name) {
            $args = [
                'sort' => $sortBy,
                'sortby' => isset($_GET['sortby']) && $_GET['sortby'] == 'asc' ? 'desc' : 'asc',
            ];
            if (isset($_GET['page'])) {
                $args['page'] = $_GET['page'];
            }
            $sort[$name] = '/index.php?' . http_build_query($args);
        }
        return $sort;
    }
}