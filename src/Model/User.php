<?php

/**
 * Модель - Пользователь.
 *
 * Class User
 */
class User
{

    private $admin;
    private $guest;
    private $db;

    public function __construct()
    {
        $this->admin = 'admin';
        $this->guest = 'Гость';
        $this->db = new DB();
    }

    /**
     * Проверка на админ.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return self::isLogged() && $_SESSION["name"] == $this->admin;
    }

    /**
     * Если залогиненный.
     *
     * @return bool|mixed
     */
    function isLogged()
    {
        if (isset($_SESSION["is_auth"])) {
            return $_SESSION["is_auth"];
        } else return false;
    }

    /**
     * Получаем имя залогиненного пользователя.
     *
     * @return mixed|string
     */
    public function getName()
    {
        return self::isLogged() ? $_SESSION["name"] : $this->guest;
    }

    /**
     * Получаем почту залогиненного пользователя.
     *
     * @return mixed|string
     */
    function getEmail()
    {
        $name = self::getName();

        $query = $this->db->query('SELECT u.email FROM tz_users AS u WHERE name="' . $name . '"');
        if (
            isset($query[0]['email']) &&
            $name !== $this->guest
        ) {
            return $query[0]['email'];
        } else {
            return '';
        }
    }

    /**
     * Получаем имя пользователя по ID задачи.
     *
     * @param $taskID
     * @return int|mixed
     */
    function getUserNameByTaskID($taskID)
    {
        $query = $this->db->query('SELECT u.name FROM tz_users AS u LEFT JOIN tz_tasks AS t ON t.user_id = u.id WHERE t.id="' . $taskID . '"');
        if (isset($query[0]['name'])) {
            return $query[0]['name'];
        } else {
            return $this->guest;
        }
    }

    /**
     * Получаем ID пользователя по его почте.
     *
     * @param $email
     * @return int|mixed
     */
    function getUserIDbyEmail($email)
    {
        $query = $this->db->query('SELECT u.id FROM tz_users AS u WHERE email="' . $email . '"');
        if (isset($query[0]['id'])) {
            return $query[0]['id'];
        } else {
            return 0;
        }
    }

    /**
     * Проверка авторизации.
     *
     * @param $name
     * @param $pwd
     * @return bool|$name
     */
    function checkLogin($name, $pwd)
    {
        $query = $this->db->query('SELECT u.name, u.email FROM tz_users AS u WHERE name="' . $name . '" AND pass="' . md5($pwd) . '"');
        if (isset($query[0]['name'])) {
            return $name;
        } else {
            return FALSE;
        }
    }

    /**
     * Удаляем пользователей.
     */
    function deleteUsers()
    {
        $this->db->query(
            'DELETE FROM tz_users WHERE id!=1'
        );
    }

    /**
     * Обновляем ник, почту юзера.
     *
     * @param $values
     */
    function updateUser($values)
    {
        $name = $values['name'];
        $email = $values['email'];
        $userID = $values['userID'];
        $this->db->query(
            'UPDATE tz_users SET name=\'' . $name . '\', email=\'' . $email . '\' WHERE id='
            . $userID
        );
    }

    /**
     * Добавляем пользователя.
     *
     * @param $name
     * @param $email
     */
    function addGuest($name, $email)
    {
        $this->db->query("INSERT INTO tz_users (name, email) VALUES('$name', '$email')");
    }

}