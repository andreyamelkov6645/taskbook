<?php

/**
 * Модель - Страница (хелперсы).
 *
 * Class Page
 */
class Page {

    /**
     * Валидация данных и вывод ошибок.
     *
     * @param $values
     * @return bool
     */
    static function validateValues($values, $view) {

        $name = isset($values['name']) && !empty($values['name']) ? $values['name'] : FALSE;
        $pwd = isset($values['pwd']) && !empty($values['pwd']) ? $values['pwd'] : FALSE;
        $email = isset($values['email']) && !empty($values['email']) ? $values['email'] : FALSE;

        switch ($view) {
            case 'reg':
                if (!$name || !$pwd || !$email) {
                    Page::addMessage("Заполните все поля.", 'danger');
                    return FALSE;
                }
                if ($name && $pwd && self::validateEmail($email)) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            break;

            case 'login':
                if (!$name || !$pwd) {
                    Page::addMessage("Заполните все поля.", 'danger');
                    return FALSE;
                } else {
                    return TRUE;
                }

            case 'task':
                if (!$name || !$email) {
                    Page::addMessage("Заполните все поля.", 'danger');
                    return FALSE;
                } else {
                    return TRUE;
                }

            break;
        }
    }

    /**
     * Валидация почты.
     *
     * @param $email
     * @return bool
     */
    static function validateEmail($email) {
        if ($email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) !== FALSE) {
                return TRUE;
            } else {
                Page::addMessage('Введите почту в правильном формате.', 'danger');
                return FALSE;
            }
        } else {
            Page::addMessage('Введите почту.', 'danger');
            return FALSE;
        }
    }

    /**
     * Уведомления.
     *
     * @param $message - сообщение.
     * @param $type - ошибка либо инфо.
     */
    public static function addMessage($message, $type = 'info')
    {
        $_SESSION['message'] = [
            'message' => $message,
            'type' => $type,
        ];
    }



}