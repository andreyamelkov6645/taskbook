<?php

/**
 * Модель - Задача.
 *
 * Class Task
 */
class Task
{

    private $db;

    function __construct()
    {
        $this->db = new DB();
    }

    /**
     * Получаем список задач.
     *
     * @param $params - аргументы для сортировки и пейджера.
     * @return array
     */
    function getTasks($params)
    {

        $offset = Pager::ITEMS_IN_PAGE;

        $query = "SELECT t.*, u.name, u.email FROM tz_tasks AS t LEFT JOIN tz_users AS u ON t.user_id=u.id";

        $query .= $params['sort'] ? " ORDER BY " . $params['sort'] : '';
        $query .= $params['sortBy'] ? ' ' . $params['sortBy'] : '';

        if (isset($params['page'])) {
            $page = ($params['page'] - 1) * $offset;
            $query .= " LIMIT " . $page . ',' . $offset;
        }

        return $this->db->query($query);
    }

    /**
     * Получаем количество задач.
     *
     * @return int|mixed
     */
    function getCount()
    {
        $result = $this->db->query(
            'SELECT COUNT(*) AS count FROM tz_tasks'
        );

        return isset($result[0]['count']) ? $result[0]['count'] : 0;
    }

    /**
     * Фильтрация входных данных.
     *
     * @param $values
     */
    function filterValues(&$values)
    {
        if (!empty($values) && is_array($values)) {
            foreach ($values as $key => $value) {
                switch ($key) {
                    case 'task_id':
                        $values[$key] = intval($value);
                        break;

                    case 'task_text':
                    case 'email':
                        $input_text = strip_tags($value);
                        $input_text = htmlspecialchars($input_text);

                        $values[$key] = $input_text;
                        break;
                }
            }
        }
    }

    /**
     * Удаляем задачи.
     */
    function deleteTasks()
    {
        $this->db->query('DELETE FROM tz_tasks');
    }

    /**
     * Удаляем задачу.
     *
     * @param $taskID
     */
    function deleteTask($taskID)
    {
        $this->db->query(
            'DELETE FROM tz_tasks WHERE id=' . $taskID
        );
    }

    /**
     * Сохрание статуса задачи после перезагрузки страницы редактирования.
     *
     * @param $values
     */
    function saveData($values) {
        $_SESSION['task'] = [
            'status' => $values['status'],
            'id' => $values['id'],
        ];
    }

    /**
     * Обновляем задачу.
     *
     * @param $values
     * @throws Exception
     */
    function updateTask($values)
    {
        $this->filterValues($values);
        $status = isset($values['task_status']) && $values['task_status'] == 'on' ? 1 : 0;
        $text = $values['task_text'];
        $taskID = $values['task_id'];

        $user = new User();
        $userID = $user->getUserIDbyEmail($values['email']);

        // Создаем гостя.
        if (!$userID) {
            $user->addGuest($values['name'], $values['email']);
            $userID = $user->getUserIDbyEmail($values['email']);
        } else {
            $values['userID'] = $userID;
            $user->updateUser($values);
        }

        $task = new Task();
        $oldTask = $task->getTask($taskID);
        $oldText = $oldTask['text'];
        $adminEdit = $text === $oldText && $oldTask['admin_edit'] == 0 ? 0 : 1;

        $this->db->query(
            'UPDATE tz_tasks SET text=\'' . $text . '\', status=\'' . $status . '\', admin_edit=\'' . $adminEdit . '\' , user_id=\'' . $userID . '\' WHERE id='
            . $taskID
        );

    }

    /**
     * Получаем обьект задачи.
     *
     * @param $taskID - ID задачи.
     * @return mixed
     * @throws Exception
     */
    function getTask($taskID)
    {
        $result = $this->db->query('SELECT t.*, u.email FROM tz_tasks AS t LEFT JOIN tz_users AS u ON t.user_id=u.id WHERE t.id=' . $taskID);
        if (isset($result[0]["id"])) {
            return $result[0];
        } else {
            throw new \Exception("Broken task ID.");
        }
    }

    /**
     * Добавление задачи.
     *
     * @param $values - POST данные.
     */
    function addTask($values)
    {

        // Фильтрация.
        $this->filterValues($values);

        $user = new User();
        $userID = $user->getUserIDbyEmail($values['email']);

        // Создаем гостя.
        if (!$userID) {
            $user->addGuest($values['name'], $values['email']);
            $userID = $user->getUserIDbyEmail($values['email']);
        }

        $text = $values['task_text'];
        $status = isset($values['task_status']) ? 1 : 0;
        $this->db->query(
            "INSERT INTO tz_tasks (user_id, text, status) VALUES('$userID', '$text', '$status')"
        );

        Page::addMessage('Задача успешно дабавлена.');
    }

}