<?php

/**
 * Модель - Пейджер.
 *
 * Class Pager
 */
class Pager
{
    const ITEMS_IN_PAGE = 3;

    /**
     * Получаем ссылки для пейджера.
     *
     * @param $input - GET запрос.
     * @param $entriesCount - количество задач.
     * @return array
     */
    public static function getQueries($input, $entriesCount)
    {
        $queries = array();
        $pageCount = ceil($entriesCount / self::ITEMS_IN_PAGE);
        for ($i = 1; $i <= $pageCount; $i++) {
            $queries[$i] = self::getPaginationQuery($input, $i);
        }

        return $queries;
    }

    /**
     * Генерация ссылок пейджера.
     *
     * @param $input - GET запрос.
     * @param int $page - номер страницы.
     * @return string
     */
    private static function getPaginationQuery($input, $page = 1)
    {
        if (is_int($page)) {
            $input['page'] = $page;
        } else throw new \InvalidArgumentException('Parameter is not int.');

        $args = [];

        if (isset($_GET['view'])) {
            $args['view'] = $_GET['view'];
        }

        if (isset($_GET['sort'])) {
            $args['sort'] = $_GET['sort'];
        }

        if (isset($_GET['sortby'])) {
            $args['sortby'] = $_GET['sortby'];
        }

        $args['page'] = $page;

        return 'index.php?' . http_build_query($args);

    }

}