<?php

/**
 * Подключение, запрос к БД.
 *
 * Class DB
 */
class DB {

    private $user;
    private $password;
    private $dbname;
    private $hostname;
    private $mysql;

    public function __construct()
    {
        $this->user = 'user';
        $this->password = 'password';
        $this->dbname = 'dbname';
        $this->hostname = 'localhost';

        $this->mysql = mysqli_connect($this->hostname, $this->user, $this->password, $this->dbname);
        if ($this->mysql == false){
            die("Ошибка: Невозможно подключиться к MySQL " . mysqli_connect_error());
        }

    }

    /**
     * Запрос к БД.
     *
     * @param $query
     * @return array
     */
    public function query($query) {
        $result = mysqli_query($this->mysql, $query);
        if ($result) {
            $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $rows;
        }
        else {
            die("Ошибка " . mysqli_error($this->mysql));
        }
    }

}