<?php

/**
 * Задачник.
 *
 * Class TaskBookController
 */
class TaskBookController {

    public function __construct()
    {
        session_start();
        spl_autoload_register(function ($class) {
            foreach ([
                         '/src/',
                         '/src/Controller/',
                         '/src/Views/',
                         '/src/Model/',
                     ] as $path) {
                $path = ROOT. $path . $class . '.php';
                if (is_file($path)) {
                    include_once $path;
                }
            }
        });
    }

    /**
     * Разбор аргументов адресной строки, запуск нужного Контроллера.
     */
    public function run() {
        $router = new Router();
        $router->run();
    }
}