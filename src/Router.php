<?php

/**
 * Маршрутизатор.
 *
 * Class Router
 */
class Router {

    public $view;
    public $action;
    public $taskID;

    public function __construct()
    {
        $this->view = isset($_GET['view']) && is_string($_GET['view']) ? $_GET['view'] : 'list';
        $this->action = isset($_GET['action']) && is_string($_GET['action']) ? $_GET['action'] : FALSE;
        $this->taskID = isset($_GET['id']) && intval($_GET['id']) > 0 ? $_GET['id'] : FALSE;

        // Чистим временный статус задачи на странице редактирования.
        if (!in_array($this->action, ['edit', 'save-edit'])) {
            $_SESSION['task'] = [];
        }
    }

    /**
     * Разбор адресной строки, запуск роутера.
     *
     * @throws Exception
     */
    public function run()
    {

        // Действия.
        switch ($this->action) {

            case 'logout':
                $controller = new RegController();
                $controller->logoutAction();
            break;

            case 'auth':
                $controller = new RegController();
                $controller->authAction();
                break;

            case 'addtask':
                $controller = new TaskController();
                $controller->newTaskAction($_POST);
                break;

            case 'update':
                $controller = new TaskController();
                $controller->updateAction($_POST);
                break;

            case 'clear':
                $controller = new TaskController();
                $controller->clearAction();
                break;

            case 'save-edit':
                $controller = new TaskController();
                $controller->saveEditAction($_POST);
                break;
        }

        // Страницы и действия.
        switch ($this->view) {

            case 'list':
                $controller = new ListController();
                $controller->start();
            break;

            case 'task':
                $controller = new TaskController();

                switch ($this->action) {
                    case 'edit':
                        if ($this->taskID) {
                            $controller->editPage($this->taskID);
                        }
                    break;

                    case 'delete':
                        if ($this->taskID) {
                            $controller->deleteAction($this->taskID);
                        }
                    break;

                    default:
                        $controller->addPage();
                    break;
                }

            break;

            case 'login':
                $controller = new RegController();
                $controller->loginPage();
                break;

            default:
                die(404);

        }

    }
}